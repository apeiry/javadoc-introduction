\documentclass{article}
\usepackage[utf8]{inputenc}

% Useful packages
    \usepackage{verbatim}
    \usepackage[a4paper,top=2cm,bottom=2cm,left=1.2cm,right=1.2cm,marginparwidth=1cm]{geometry} % Set page size and margins
    \usepackage{float} % Used for table (fixed position in the text)
    \usepackage{hyperref}
    
\hypersetup{
    colorlinks=true,
    linkcolor=blue,   
    urlcolor=blue,
    }

\title{Tutoriel pour la Javadoc pour étudiants}
\author{André Peiry}
\begin{document}
\maketitle



\section{Introduction}
    Le language de ce document est le français. Si vous cherchez la version anglais, elle est disponible à \url{https://gitlab.epfl.ch/apeiry/javadoc-introduction} pour participer et corriger les erreurs.

    L'IDE utilisé comme example est IntelliJ Ultimate Edition 2022.2.1 (ce qui ne change rien à la théorie de ce tutorial, seulement l'esthétisme).
  
    \paragraph{Rappelez-vous que votre code est du texte} avant d'être du code\footnote{Ce qui est "ridiculeusement" vrai, vu que les fichiers Java ou C ne sont que des .txt avec une déguisement. Votre code n'est pas lisible par une machine, il doit d'abord être compilé (traduit).}. La fameuse citation \emph{Quand j'ai écrit ce code, seul Dieu et moi le comprenions. Maintenant, seul Dieu sait}\footnote{"When I wrote this code, only God and I understood it. Now, only God knows ..."} n'est pas fameuse pour rien. La plupart de vos fonctions seront longues de 5 à 10 lignes (ou moins pour les getters et setters), mais quand vous écrivez une fonction de plus de 30 lignes, vous ne voulez vraiment pas re-comprendre ce que vous avez écrit parce que cela vous prendra trop de temps, alors que vous savez que ce code marche. Commenter votre code ne vas pas non plus réduire ses performance, car vos commentaires sont ignorés par le compilateur.

    Ne pensez pas ce que ça ne vous arrivera pas. J'ai littéralement écrit mon premier \emph{Je ne sais pas comment ça marche} dans mon mini-projet 1 (oui, je travaillais sur du multithreading à 2h du matin, c'est quoi votre question ?). Lesson apprise : Le code de 2h du matin est le meilleur et le pire code que vous ne produirez jamais (et encore pire si vous ne le commentez pas).
    
    \paragraph{Petit avertissement} Je suis connu pour abusez le \verb!-v! dans le terminal Linux autant que quand je parle. Alors voici un message de l'auteur avec qu'il n'écrive : ça va être long, et je souhaite à mon future moi beaucoup de courage pour la relecture.
    
    

\section{Commentaire}
    \subsection{Commentaire en ligne}
        Un commentaire en ligne est utilisé pour décrire un petite portion de code, de manière à ce que 
        \begin{verbatim}MyInterface f = b -> (b & (0xCAFEBABE >> 31)) == (1 | 15 >> 3);\end{verbatim}
        peut être lue facilement si vous ajouter \verb!// Check if a number is odd.! at the end of the line. 

        Si votre commentaire est trop long pour la fin de la ligne, placez-le sur la ligne en-dessus de la ligne à décrire.

        C'est un petit détail d'esthétisme, je sais, mais en mettant un espace entre le \verb!//! et votre commentaire rend votre commentaire tellement\footnote{Par approximativement $492\%$.} plus lisible .

        Vous verrez plus tard dans ce tutoriel que vous pouvez écrire un TODO avec cela, mais cela peut devenir assez long, et vous voudrez donc utilisez ...
        
        
    \subsection{Commentaire multiligne}
        Lorsque vous avez besoin de plus d'une ligne pour décrire votre code (ou si vous voulez désactiver le misérable code que vous venez d'écrire parce que pourquoi marcherait-il la cinquième fois ?!?), vous devrez en venir à utiliser une manière plus sérieuse d'écrire votre texte, et ce sont les commentaires multilignes. Pour les utilisez, écrivez \begin{verbatim}
/*
 * This is my comment. Please remember I'm a decent being and 
 * that I deserve a final point. If you can't put me on one line,
 * please avoid breaking my line in the middle of a sentence.
 * You have to be consice but precise when speaking to somebody else through me.
 */\end{verbatim}
        
    \subparagraph{Deuxième usage} Si une fonction atteint un certain nombre de ligne, vous pouvez la séparer votre code avec des petits 
        \begin{verbatim}
/* Detecting and resetting invalid parts. */
... code ...

/* Interpolating holes. */
... code ...

/* Verifying data integrity. */
... code ...\end{verbatim}

        Ceci est également faisable avec des commentaires en ligne (simple), mais ce sera plus lisible à cause du "Wow" de la syntax du commentaire multiligne.
        
    \subparagraph{Atrocité}
        S'il vous plait, retenez vous d'écrire
            \begin{verbatim}void update() { updatePlayer(player /* The player */, null, 0); }\end{verbatim}
        
    \subsection{Commentaire de JavaDoc}
        En quelques mots, la JavaDoc est une façon d'expliquer une classe, constructeur, méthode, fonction et variable avec du texte qui vous lui attachez. Vous vous demandiez d'où venait les informations ultra-sympa qui apparaissait quand vous survoliez une fonction dans un IDE ? Vous pouvez faire ça également ! Tout ce que vous avez à faire, c'est d'ajouter un commentaire multiligne avant la déclaration, mais avec avec une deuxième étoile sur la première ligne.
        
        Ici, un petit example pour vous montrer une classe bien documentée, mais ce n'est qu'une démonstration pour cette section. Un exemple plsu complet vous sera donné à la fin.
        \begin{verbatim}
/**
 * This utility class provide basic math functions for matrices.
 */
 public final class MatrixMath{
 
    /** Private constructor to avoid instanciation. */
    private MatrixMath() { }
    
    /**
     * Scale a matrix by a non-zero scalar.
     * @param m The {@link Matrix} to scale.
     * @param scalar That scalar that should be applied to the matrix.
     * @return A scaled version of the input matrix.
     */
     public static Matrix scaleMatrix(Matrix m, int scalar){
        Matrix newMatrix;
        ... code ...
        return newMatrix;
     }
 }\end{verbatim}
        
        Si la description est assez courte, vous pouvez utiliser la syntax du constructeur privé dans l'exemple ci-dessus.
        
        \paragraph{[IntelliJ] Raccourci} Écrivez simplement \verb!/**! en-dessus de votre fonciton à être commenté et taper \verb!Enter!, et il devrait vous la générer automatiquement.


\section{Tags}
    À la fin de cette magnifique documentation, vous devrez spécifiez une ou deux choses.
    
        \paragraph{@deprecated} Indique que cette classe, constructeur, fonction, méthode ne devrait pus être utilisée. L'explication devrait expliquer comment la contourner.
    
    \subsection{Tags de classe}
        Ce sont les tags qui seront utiles uniquement pour les documentations de classe.
        
        \paragraph{@author} Nomme l'auteur qui a écrit le code. À l'EPFL, on vous demandera de votre nom, prénom et sciper. Il n'y a pas de standard là-dessus, mais je suggeste de l'écrire comme suit \verb!@author André Peiry (123456)!. Comme les partenaires peuvent être décevant, je recommande fortement qui vous n'écriviez votre nom que sur les classes que vous avez écrit ou aidé à écrire de manière significative, de manière à ce que si vous veniez à vous séparer, il vous sera plus facile à récupérer votre partie du dur labeur.
        \paragraph{@since} Vous ne l'utiliserez pas énormément, car il s'agit de mentionner la version depuis laquelle la fonction existe, très utilisé dans les librairies public.
        
    \subsection{Tags de fonctions}
        \paragraph{@param} Décrit un paramètre pour un constructeur, fonction ou méthode.
        \paragraph{@return} Décrit ce qui est retourné par la fonction.
        \paragraph{@throws} Décrit quel exception peut être lancée par l'appel de ce constructeur, fonction ou méthode.
    
    \subsection{Tags d'enjolivement}
        \paragraph{@code} SI vous avez besoin d'écrire du code ou des équations mathématique, utiliser ce tag \verb!{@code 1+1=2}!. Si vous avez besoin de montrer un block entier de code, vous pouvez le mettre entre \emph{tag} \verb!<pre> ... </pre>!\footnote{Notez qu'il est légèrement plus compliqué de manipulier cette environnement, étant donné que tout (espace, retour à la ligne) ce qui est entre les tags est pris en compte}.
        
        \paragraph{@inheritDocs} Si votre fonction masque une autre, vous pouvez simplement remplacer la documentation avec \verb!/** @inheritDoc */!. 
        
        \paragraph{@link} Si la compréhension d'une classe, constructeur, fonction ou méthode requiert d'être liée à une autre que vous avez écrit dans votre programme, reférez-vous avec \verb!{@link MyAwesomeClass}! Pour lier à une variable, constructeur, fonction ou méthod, utilisez \verb!Class#variableName! ou \verb!Class#function!, avec ou sans les parenthèses.
        
        \paragraph{@see} Similaire à \verb!link!, mais comme une note de fin de page.
        
        \paragraph{@value} Si, à la place d'un lien (pour que l'utilisateur puisse voir la documentation), vous voulez montrer la valeur d'une variable (\verb!static final!). De la même manière que \verb!link!, les membres externes d'une classe devrait être appelé avec \verb!Class#MY_CONSTANT!.
        
    \subsection{Example}
        \begin{verbatim}
/**
 * This utility class provide basic math functions for matrices.
 * @author André Peiry (123456)
 * @see Vector
 * @since 1.0
 */
 public final class MatrixMath{
 
    /** Private constructor to avoid instanciation. */
    private MatrixMath() { }
    
    /**
     * Scale a matrix by a non-zero scalar.
     * If you need to multiply by {@code 0}, best is to call a
     * {@link Matrix#ZeroMatrix} instead.
     * @param m      The matrix to scale.
     * @param scalar The scalar to multiply the matrix by. Can't be {@code 0}.
     * @return A scaled {@link Matrix}.
     * @throws IllegalArgumentException if {@code scalar} is {@code 0}.
     * @see Vector#scaleBefore
     */
     Matrix scaleMatrix(Matrix m, int scalar){
        ... code ...
        return new Matrix(...);
     }
 }\end{verbatim}
        
        \paragraph{Oui, mais} ça semble être très long et compliqué, et je suis fénéant, et je ne partagerai jamais ce code, et ... Blah, blah, blah. Je ne veux rien entendre ! Oui, la plupart de la documentation est suboptimale (pensez au getters et setters), et passablement redondante (lisez le \verb!@param! de l'exemple ci-dessus), mais je vous assure que votre code roulera en $\Theta(1)$, et sera bien plus agréable pour travailler avec, et les autre personne (ainsi que votre futur vous)  seront bien plus efficaces car ils n'auront pas besoin de comprendre votre code, vu qu'ils n'auront plus besoin de comprendre votre code.

        \paragraph{[IntelliJ] Présentation de la Javadoc} Vous pouvez, en pressant l'icône "Trois lignes" qui apparait sur les numéros des lignes quand vous les survolez votre JavaDoc, présenter votre commentaire de manière à ce qu'il devienne bien plus lisible (comme une présentation HTML).


\section{Beauté du code}
    \paragraph{Longeur de votre code}
        \emph{À mon époque}, les moniteurs ne pouvaient afficher que 80 caractères par ligne, et tel était la limite de caractères par ligne de code. De nos jours, vous pouvez facilement dépasser cette limite, mais pas trop. Oui, il n'y a pas de limite "officielle"\footnote{La limite officielle est de 80 caractères, mais personne ne l'applique.} caractères par ligne. Cependant, si vous voulez (et vous en aurez besoin) utiliser un environnement mutli-fenêtre (afficher les conflits de fusion, chercher pour une partie de votre code, écrire des tests et les débugger, ...), vous ne voulez pas que votre code soit sur des lignes longues de 240 caractères. Pour ceci, il vous est fortement conseiller de les garder sous :
        \begin{table}[H] \centering \begin{tabular}{c c c}
            Originale & Moyen & Maximum \\ \hline
            80 & 100 & 120
        \end{tabular} \end{table}

        Si vous voyez que votre code est passablement petit, choisissez 80, et quand vous avez des variables de longeur  \verb!entryTicketPriceForChildrenWithVeteranDiscount!, 120 semble être un meilleur choix. Tout ce que vous voulez, c'est d'être consistent à l'intérieur d'une classe.
        
        \subparagraph{[IntelliJ] Ajouter des guides visuels}
            \verb!Settings > Editor > Code Style > General # Visual Guide! et \\ \verb!# Hard wrap at!. Pour les guides visuels,  sélectionnez \verb!80, 100! pour que vous les voyez à tout moment. Pour le retour à la ligne forcé, vous pouvez le laisser à \verb!120!, il vous sert également de guide vertical, mais plus contrasté. 
        
    \paragraph{Satisfaction verical}
        Lequel préférez-vous ?
        \begin{table}[H]
            \centering
            \begin{tabular}{l|l}
                \verb!int a = 8;! & \verb!int a       = 8;! \\
                \verb!float time = 5.f;! & \verb!float  time = 5.f;! \\
                \verb!Matrix m = Matrix.ZeroMatrix();! & \verb!Matrix m    = Matrix.ZeroMatrix();!
            \end{tabular}
        \end{table}
        Ai-je vraiment besoin d'expliquer ? Gardez juste en tête de les grouper, de manière à ne pas devoir aligner \\ \verb!protected MySuperLongNameClass mySuperLongVariableName = new ...! avec un simple \verb!int a = 0;!
        
    \paragraph{Hiérarchie d'accès}
    
        Dans chaque classe, l'ordre suivant devrait être respécté :
        \begin{table}[H] \centering
            \begin{tabular}{c|c}
                \verb!static! & \verb!public! \\
                 & \verb!protected! \\
                 & package private (no modifiers) \\
                 & \verb!private! \\ \hline
                non-\verb!static! & \verb!public! \\
                 & \verb!protected! \\
                 & package private (no modifiers) \\
                 & \verb!private!
            \end{tabular}
        \end{table}
        
        Une fois que les variables on été déclarées, le reste devrait respécter :
        \begin{table}[H] \centering
            \begin{tabular}{c|c}
                Constructeurs & Du plus au moins utilisé \\ \hline
                Méthodes et functions & Groupés par fonctionnalité
            \end{tabular}
        \end{table}
        Notez que, même groupées, l'ordre devrait au moins montrer les fonctionnalités \verb!public! du groupe avant les autres.
        
    \paragraph{Déclaration d'import}
        Un petit travail (qui est souvent faite automatiquement par votre IDE) est d'ordonner vos déclarations d'imports. qui sont dans l'ordre suivant :
        \begin{table}[H]\centering
            \begin{tabular}{c|c}
                Non-\verb!static! & importations du même projet \\
                 & importations Java \\ \hline
                \verb!static! & importations du même projet \\
                 & importations Java
            \end{tabular}
        \end{table}
        
        Nul besoin de préciser qu'elles doivent être groupé par paquet, et que dans le cas où il y a énormément d'import, sentez-vous libre d'ajouter une ligne blanche entre deux importations de paquet.
        
    \begin{verbatim}
package ch.epfl.math;

import ch.epfl.math.Matrix;
import ch.epfl.math.Vector;

import java.io.IOExceptions;

import static java.lang.Math.max;\end{verbatim}

    \paragraph{Contrôle du flux}
        Une boucle \verb!for! décrit une boucle dans laquelle on connait l'itération. Une boucle \verb!while! décrit une boucle dans laquelle on ne sait pas quand elle se finira. Une boucle \verb!do-while! est une boucle \verb!while! qui devrait au moins s'excecuter une fois. Gardez cela en tête quand vous écriter des boucle imbriqués comme dans l'exemple suivant. J'ai laisser le contrôle du flux pour garder l'exemple court. Vous pouvez observer une flambé de manipulation sur la variable \verb!index! tout au long de la partie du milieu, avec des boucles  \verb!for! sans initialisation. Mais encore une fois, cela rend le code plus facile à comprendre, comme l'on sait dans quelle boucle nous somme.
        \begin{verbatim}
for (int i = 0; i < n; i++) { ... }

if (...) { while (index != n && ...) index++; }

for ( ; index < n - 1; index++) {
    if (...) { while (index != n && ...) index++; }
}

if (...) {
    if (...) { ... }
    if (...) { ... }
    for (int i = 0; i < s); i++) {
        for (int x = arrayList.get(i)[0]; x < arrayList.get(i)[1]; x++) { ... }
    }
}\end{verbatim}

        \subparagraph{Éviter l'imbrication lorsque possible}
            Voici deux code. Lequel est le meilleur ?
            \begin{verbatim}
boolean hasAdminAccess(Person p) {}
    if(a) {
        if(b) {
            if(c) {
                if(d) {
                    ...
                        return true;
                    ...
                }
            }
        }
    }
    return false;
}\end{verbatim}
            Or 
            \begin{verbatim}
boolean hasAdminAccess(Person p) {}
    if(!a) return false;
    if(!b) return false;
    if(!c) return false;
    if(!d) return false;
    ...
    return true.
}\end{verbatim} 

            Comme vous pouvez le voir, il devient particulière inadéquat d'écrire des imbrication de condition (à supposez qu'on ne puisse pas les simplifier).



\section{Convention de nommage}
      Le nom de variables, fonctions et méthodes sont définies par la casse chameau, ou \verb!camelCase!\footnote{(c'est la bosse d'un chameau, vous l'avez ?)}. Les fonctions et méthodes devrait être décrite avec un verbe (\verb!update()!, \verb!isPrime()!, \verb!killAllNearbyDemons()!) et les variables avec un nom commun  (\verb!myAge!, \verb!anxietyLevel!). La petite ambiguité est pour les variables et fonctions booléennes, car leur nom \verb!adminAccess! ne décrit pas réellement ce qu'elles font. À la place, il est acceptable de les renommer, \verb!isAdmin! ou \verb!hasAdminAccess! (la plupart du temps, vous pouvez simplement appeler la fonction associée à la place d'utiliser une variable).
        
        Les variables constantes n'utilisent pas la casse chameau, mais sont tout en majuscules avec un tiret bas (\verb!_!) entre les mots : \verb!A_CONSTANT!.
        
        Il est aussi à savoir que les classes et constructeurs sont en casse chameau, mais avec une majuscule au début : \verb!MyClass! et son constructeur \verb!MyClass()!
        
        \paragraph{Unité} Pour dénoter une unité physic à une variable, il faut l'ajouter (après un tiret bas) à la fin de la variable : \verb!mass_kg!.
        
    \subsection{Ordre d'accession}
        Est-ce \verb!static public! ou \verb!public static! ?
        \begin{verbatim} 
[access] [static] [final] [type] variableName;
 public   static   final   int   HOURS_WASTED_WRITING_THIS;
 private                   float myHeight;\end{verbatim}



\section{TODOs pour le travail collaboratifs}
    Il n'y a pas de nom pour une telle chose, mais les plus communs sont les TODOs, et nous utiliserons ce nom comme suit.
    
    Ces TODOs sont très pratiques quand vous n'avez pas le temps de travailler, mais que vous avez quand même envie d'avancer. Alors, vous pouvez revoir le code de votre partenaire, et vous pouvez ajouter \verb!// TODO Simplify this part by avoiding intermadiate variables.!, ou si vous voyez un meilleur moyen/algorithme pour écrire cette fonction.
        
    Vous pouvez également l'utiliser pendant que vous écrivez, car vous voulez finir d'écrire une première chose, alors que vous êtes soudainement frappé d'une illumination, mais vous encore la moitié de votre esprit sur ce que vous travaillez déjà. Avec ce genre de TODO, vous pouvez rapidement revenir à ce que vous faisiez, avec quelques instruction.
    
    
    \subsection{[IntelliJ] Créer vos propres TODOs}
        \verb!File > Settings > Editor > TODO! : Pour créer un nouveau TODO personel, appuyez sur le \verb!+! en dessus des deux déjà définis (\verb!TODO! et \verb!FIXME!). La liste que j'avais était la suivante, mais sentez-vous libre le créer la votre.
        J'ai été assez gentil pour vous laisser les détails "graphiques".
        
        Notez que dans IntelliJ, vous avez un menu en bas à droite de votre fenêtre (à côté de Git si vous l'utilisez). Dans ce panneau, vous avez accès à tous les TODOs (dont ceux que vous avez créez vous-même) qui se trouve dans le projet.
        
        \subparagraph{Détails}
            Les \verb!\b!s sont là pour délimiter le modèle de la chaine de caractères du TODO, les \verb!.*! sont utilisé pour appliquer au commentaire en entier. De cette manière, votre \emph{nom} devrait ressembler à \verb!\bnom\b*!
        
        \paragraph{TODO}
            Pour les choses qui sont encore à faire (implémentation, petit bug à résoudre, cas limite à ajouter).
            
            Laisser comme par défault.
            
        \paragraph{FIXME}
            Pour les choses que ne marchent pas, et qui doivent être corrigées.
            
            Laisser comme par défault.
            
        \paragraph{XXX}
            Pour quand quelque chose marche, mais vous ne savez pas pourquoi (ou vous avez trop peur de toucher ce code).
            
            \begin{table}[H]\centering\begin{tabular}{c c c c}
                Icon & Foreground & Background & Effects \\ \hline
                ! & \verb!#FFFFFF! & \verb!#FF0000! & \verb!#9C0031!
            \end{tabular}\end{table}
            
        \paragraph{IDEA}
            Pour quand vous avez une idée pour une meilleur implémentation, mais qui n'est pas fondamentale, ou pour laquelle vous n'avez pas le temps en ce moment.
        
            \begin{table}[H]\centering\begin{tabular}{c c c c}
                    Foreground \\ \hline
                    \verb!#008DDE!
            \end{tabular}\end{table}
        
        \paragraph{IMPORTANT}
            Pour les choses que vous ne devriez pas oublier. Je l'utilise comme suffix pour d'autre TODOs en utilisant le modèle \verb!\bimportant\b! sans la fin \verb!.*!.
            
            \begin{table}[H]\centering\begin{tabular}{c c c c}
                    Icon & Foreground & Effects \\ \hline
                    ! & \verb!#FF0000! & \verb!#FF0000!
            \end{tabular}\end{table}
        
        \paragraph{OPTIMIZATION}
            Quelque peu redondant avec \verb!IDEA!, mais plus une spécification, pour savoir si l'idée est nouvelle ou s'il s'agit d'optimiser la chose.
            
            \begin{table}[H]\centering\begin{tabular}{c c c c}
                    Foreground \\ \hline
                    \verb!#A800C0!
            \end{tabular}\end{table}
            
        \paragraph{QUESTION} Pour quand vous devez poser une question à un assistant (ou collègue) pour un sujet concernant le cours.
        
            \begin{table}[H]\centering\begin{tabular}{c c c c}
                    Icon & Foreground & Background \\ \hline
                    ? & \verb!#0000FF! & \verb!FFFFFF!
            \end{tabular}\end{table}

\section{Exemple complet}
\begin{verbatim}
package ch.epfl.math;

import ch.epfl.math.Matrix;
import ch.epfl.math.Vector;

import java.io.IOExceptions;

import static java.lang.Math.max;

/**
 * This utility class provide basic math functions for matrices.
 * @author André Peiry (123456)
 * @author Commandant Lassard
 * @author Chief Hurnst
 * @author Madame Maire
 * @author Mr. President
 * @author His Holyness the Pope
 * @author The King of Norway
 */
 public final class MatrixMath{
 
    /** A rough approximation of the famous constant. */
    static public  final float PI = 22f/7f;
    /** The speed of a tiger escaping a black hole. */
    static private final int LIGHT_SPEED_M_S = 299_792_458;
    
    /** Value of the gamma constant. 1 per definition in our universe. */
    public  double gamma = 1d;
    /** Neutral element of the addition; */
    private int additionNeutralElement = 1;
 
    /** Private constructor to avoid instanciation. */
    private MatrixMath() { }
    
     /**
      * Compute the echelon form of a matrix.
      * @param A matrix to echelon.
      * @return The echelon form of the given matrix.
      */
     public Matrix echelonForm(Matrix m) {
        /* Reduce the first row. */
        ... code ...
        
        /* Reduce next rows. */
        while(...) { // OPTIMIZATION simplify this loop.
            ... even more code ...
        }
        
        /* Coherence verification. */
        /*
         * IMPORTANT IDEA Do we really need to
         *  verify this if we prove that previous
         *  operation keep the data intact ?
         */
        ... what could be here ? ...
        
        return new Matrix(...);
     }
     
     /**
      * Normalized a row of a matrix.
      * @param m   The matrix in which the operation 
      *            should take place.
      * @param row The index of the row to normalized.
      * @return The same matrix, but with a row normalized.
      // TODO Could be public ?
     private Matrix rowNormalized(Matrix m, int row) {
        ... code ...
        // XXX Compensate a approximation.
        ...rest of the code ...
        return new Matrix(...);
     }
     
    
    /**
     * Scale a matrix by a non-zero scalar.
     * If you need to multiply by {@code 0}, best is to call a
     * {@link Matrix#ZeroMatrix} instead.
     * @param m      The matrix to scale.
     * @param scalar The scalar to multiply the matrix by. Can't be {@code 0}.
     * @throws IllegalArgumentException if {@code scalar} is {@code 0}.
     * @return A scaled {@link Matrix}.
     */
     // QUESTION Should this really be public ?
     public Matrix scaleMatrix(Matrix m, int scalar) {
        ... code ...
        return new Matrix(...);
     }
 }\end{verbatim}



\section{Pour aller plus loin} La Javadoc est capable de présenter de l'HTML. Donc vous pouvez organizer votre documentation avec des \verb!<h1>! et autres titres, et créer des tables. Les listes par point sont aussi appréciez pour les expressions booléennes et autres sorties particulières. Le plus simple est de faire des paragraph en utilisant des  \verb!<p>! à la fin dudis paragraph pour créer un nouveau (également faisable avec un \verb!<br>!).

Ce document est un résumé de \href{https://www.oracle.com/java/technologies/javase/codeconventions-contents.html}{The Code Convention for the Java Programming Language}. Sentez-vous libre d'aller le lire, comme certains détails ont été laissé de côté.

Pour les étudiant de l'EPFL en BA2 suivant le cours CS108, n'oubliez pas d'aller lire \href{https://cs108.epfl.ch/archive/22/g/style.html}{ce petit texte sur comment coder}


\end{document}
